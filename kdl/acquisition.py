#!/usr/bin/env python3.9

# Copyright (C) 2023 Collabora Ltd
#
# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the "Software"),
# to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
# OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
# THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR
# OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
# ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
# OTHER DEALINGS IN THE SOFTWARE.
#
# SPDX-License-Identifier: MIT

__author__ = "Sergi Blanch Torne"
__email__ = "sergi.blanch.torne@collabora.com"
__copyright__ = "Copyright (C) 2023 Collabora Ltd"


from .constants import (
    DEFAULT_WAIT_INTERVAL_IN_MILISECONDS,
    MINIMUM_WAITTIME_IN_MILISECONDS,
)
from ctypes import CDLL
from datetime import datetime
from .interval import SamplingInterval
from .logging import debug
import numpy as np
from .sampler import Sampler
from signal import signal, SIGINT, SIGTERM
from .timestamp import TimeStamp
from threading import Event
from typing import List


libc = CDLL("libc.so.6")


class Acquirer:
    __sampling_interval: SamplingInterval = None
    __samplers: List[Sampler] = None
    __n_samples: int = None
    __current: int = None
    __wait_time_in_microseconds: int = None
    __acq_time_start: datetime = None
    __abort: Event = None

    def __init__(self, samplers: List[Sampler]):
        self.__sampling_interval = SamplingInterval()
        self.__samplers = samplers
        self.__abort = Event()
        signal(SIGINT, self.abort)
        signal(SIGTERM, self.abort)
        self.__abort.clear()

    @property
    def samples(self) -> int:
        return self.__current

    @property
    def wait_time_in_milliseconds(self) -> int:
        return self.__wait_time_in_microseconds // 1000

    @property
    def acquisition_started_at(self) -> datetime:
        return self.__acq_time_start

    @property
    def sampling_intervals(self) -> np.array:
        return self.__sampling_interval.acquired_values

    @property
    def acquired_values(self) -> dict:
        return {sampler.name: sampler.acquired_values for sampler in self.__samplers}

    def prepare(
        self,
        n_samples: int,
        wait_time_in_milliseconds: int = None,
    ) -> None:
        assert n_samples > 0
        if not wait_time_in_milliseconds:
            wait_time_in_milliseconds = DEFAULT_WAIT_INTERVAL_IN_MILISECONDS
        assert wait_time_in_milliseconds >= MINIMUM_WAITTIME_IN_MILISECONDS
        self.__n_samples = n_samples
        self.__wait_time_in_microseconds = wait_time_in_milliseconds * 1000
        for sampler in self.__samplers:
            sampler.prepare(self.__n_samples)
        self.__sampling_interval.prepare(n_samples)

    def acquire(self) -> None:
        self.__acq_time_start = datetime.now()
        self.__current = 0
        self.__abort.clear()
        debug(f"KDL: Acquisition start at {self.__acq_time_start}")
        while self.__current < self.__n_samples:
            if self.__abort.is_set():
                debug(f"KDL: Abort event, finish the acquisition.")
                break
            # the acquisition:
            t_0 = TimeStamp(datetime.now())
            for sampler in self.__samplers:
                sampler.acquire_one(t_0)
            self.__sampling_interval.acquire_one(t_0)
            self.__current += 1
            t_diff = (TimeStamp.now() - t_0).microseconds
            next_acq_in = self.__wait_time_in_microseconds - t_diff
            if next_acq_in > 0:
                libc.usleep(next_acq_in)
        debug(
            f"KDL: Acquisition completed at {datetime.now()} with {self.__current} samples."
        )

    def abort(self, *args):
        debug(f"KDL: Signal {args[0]} received: abort")
        self.__abort.set()
