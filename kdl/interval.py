#!/usr/bin/env python3.9

# Copyright (C) 2023 Collabora Ltd
#
# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the "Software"),
# to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
# OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
# THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR
# OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
# ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
# OTHER DEALINGS IN THE SOFTWARE.
#
# SPDX-License-Identifier: MIT

__author__ = "Sergi Blanch Torne"
__email__ = "sergi.blanch.torne@collabora.com"
__copyright__ = "Copyright (C) 2023 Collabora Ltd"


from .sampler import Sampler
from .timestamp import TimeStamp


class SamplingInterval(Sampler):
    __timeinterval: int = None
    __timestamp: TimeStamp = None
    __factor: float = None

    def __init__(self, factor: float = 1e-3, *args, **kwargs):
        super(SamplingInterval, self).__init__(*args, **kwargs)
        self.__factor = factor

    @property
    def name(self) -> str:
        return f"{self.__class__.__name__}"

    @property
    def value(self) -> int:
        return self.timeinterval

    @property
    def factor(self) -> float:
        return self.__factor

    @property
    def timeinterval(self) -> int:
        return self.__timeinterval

    @property
    def timestamp(self) -> TimeStamp:
        return self.__timestamp

    @timestamp.setter
    def timestamp(self, new_timestamp: TimeStamp) -> None:
        if not self.__timestamp:
            self.__timeinterval = 0
        else:
            t_diff = (new_timestamp - self.__timestamp).microseconds
            if self.__factor:
                t_diff = int(t_diff * self.__factor)
            self.__timeinterval = t_diff
        self.__timestamp = new_timestamp

    def acquire_one(self, ts: TimeStamp) -> None:
        self.timestamp = ts
        super(SamplingInterval, self).acquire_one(ts)
